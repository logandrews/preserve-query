
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;

namespace PreserveQuery
{
    public class QueryStringLinkGenerator : LinkGenerator
    {
        private LinkGenerator _linkGenerator;

        public QueryStringLinkGenerator(LinkGenerator linkGenerator)
        {
            _linkGenerator = linkGenerator;    
        }

        public override string GetPathByAddress<TAddress>(HttpContext httpContext, TAddress address, RouteValueDictionary values, RouteValueDictionary ambientValues = null, PathString? pathBase = null, FragmentString fragment = default, LinkOptions options = null)
        {
            foreach (var queryValue in httpContext.Request.Query)
            {
                values[queryValue.Key] = queryValue.Value;
            }

            return _linkGenerator.GetPathByAddress(httpContext, address, values, ambientValues, pathBase, fragment, options);
        }

        public override string GetPathByAddress<TAddress>(TAddress address, RouteValueDictionary values, PathString pathBase = default, FragmentString fragment = default, LinkOptions options = null)
        {
            return _linkGenerator.GetPathByAddress(address, values, pathBase, fragment, options);
        }

        public override string GetUriByAddress<TAddress>(HttpContext httpContext, TAddress address, RouteValueDictionary values, RouteValueDictionary ambientValues = null, string scheme = null, HostString? host = null, PathString? pathBase = null, FragmentString fragment = default, LinkOptions options = null)
        {
            foreach (var queryValue in httpContext.Request.Query)
            {
                values[queryValue.Key] = queryValue.Value;
            }

            return _linkGenerator.GetUriByAddress(httpContext, address, values, ambientValues, scheme, host, pathBase, fragment, options);
        }

        public override string GetUriByAddress<TAddress>(TAddress address, RouteValueDictionary values, string scheme, HostString host, PathString pathBase = default, FragmentString fragment = default, LinkOptions options = null)
        {
            return _linkGenerator.GetUriByAddress(address, values, scheme, host, pathBase, fragment, options);
        }
    }
}
